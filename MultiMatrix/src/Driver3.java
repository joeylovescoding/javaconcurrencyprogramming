import java.util.ArrayList;

// Create Thread by num of processers
public class Driver3 implements Driverable{

	@Override
	public void multiply(double[][] m1, double[][] m2, double[][] ret) {
		ArrayList<Thread> arr = new ArrayList<>();
		int num = java.lang.Runtime.getRuntime().availableProcessors();
		int l = m1.length;
		int step = l / num;

		int lo = 0, hi = step;
		while(hi != l) {
			Thread t = new Thread(new Multiply3(m1, m2, ret, lo, hi));
			arr.add(t);
			t.start();

			lo = hi;
			hi += step;
			if(arr.size() == num - 1) {
				hi = l;
			}
		}

		// need to wait for all threads finish
		for(Thread trd : arr) {
			try {
				trd.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}


class Multiply3 implements Runnable{
	double[][] m1, m2, ret;
	int lo, hi, n, w;

	public Multiply3(double[][] m1, double[][] m2, double[][] ret, int lo, int hi) {
		this.m1 = m1;
		this.m2 = m2;
		this.ret = ret;
		n = m2.length;
		w = m2[0].length;
		this.lo = lo;
		this.hi = hi;
	}

	@Override
	public void run() {
		for(int i = lo; i < hi; i++) {
			for(int j = 0; j < w; j++) {
				ret[i][j] = 0;
				for(int k = 0; k < n ; k++) {
					ret[i][j] += (m1[i][k] * m2[k][j]);
				}
			}
		}
	}
}
