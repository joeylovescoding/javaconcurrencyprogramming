import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.AssumptionViolatedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Stopwatch;
import org.junit.rules.Timeout;
import org.junit.runner.Description;

public class UnitTestDriver {

	@Rule
	public Timeout globalTimeout = Timeout.seconds(10);

	@Rule
	public final Stopwatch stopwatch = new Stopwatch() {

		void printInfo(long nanos, Description description, String status) {
			String testName = description.getMethodName();
			System.out.println(String.format("Test %s %s, spent %d microseconds",
					testName, status, TimeUnit.NANOSECONDS.toMicros(nanos)));
		}

		protected void succeeded(long nanos, Description description) {
			printInfo(nanos, description, "succeeded");
		}

		protected void failed(long nanos, Throwable e, Description description) {
			printInfo(nanos, description, "failed");
		}

		protected void skipped(long nanos, AssumptionViolatedException e,
				Description description) {
			printInfo(nanos, description, "skipped");
		}

		protected void finished(long nanos, Description description) {
			printInfo(nanos, description, "finished");
			System.out.println();
		}

	};

	static final int len = 500;
	static final Random r = new Random();
	static final double[][] m1 = TestDriver.generateMatrix(len, len, r);
	static final double[][] m2 = TestDriver.generateMatrix(len, len, r);
	double[][] ret1 = new double[len][len];
	double[][] ret2 = new double[len][len];
	double[][] ret3 = new double[len][len];
	double[][] ret4 = new double[len][len];

	@Test
	public void testSerialDriver() {
		SerialDriver s = new SerialDriver();
		s.multiply(m1, m2, ret1);
	}


	@Test
	public void testEachElement() {
		Driver1 d1 = new Driver1();
		d1.multiply(m1, m2, ret2);
	}

	@Test
	public void testEachRow() {
		Driver2 d2 = new Driver2();
		d2.multiply(m1, m2, ret3);
	}

	@Test
	public void testOnNumOfProcessors() {
		Driver3 d3 = new Driver3();
		d3.multiply(m1, m2, ret4);
	}
}


/*
On len == 500

Test testEachElement failed, spent 10011494 microseconds
Test testEachElement finished, spent 10011494 microseconds

Test testOnNumOfProcessors succeeded, spent 426823 microseconds
Test testOnNumOfProcessors finished, spent 426823 microseconds


Test testSerialDriver succeeded, spent 904062 microseconds
Test testSerialDriver finished, spent 904062 microseconds

Test testEachRow succeeded, spent 371996 microseconds
Test testEachRow finished, spent 371996 microseconds
 */