import java.util.*;

//for each position create a Thread
public class Driver1 implements Driverable{

	@Override
	public void multiply(double[][] m1, double[][] m2, double[][] ret) {
		ArrayList<Thread> arr = new ArrayList<>();
		int l = m1.length, w = m2[0].length;

		for(int i = 0; i < l; i++) {
			for(int j = 0; j < w; j++) {
				Thread t = new Thread(new Multiply1(m1, m2, ret, i, j));
				arr.add(t);
				t.start();

				// to avoid too many threads
				if(arr.size() == 10) {
					for(Thread trd : arr) {
						try {
							trd.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					arr.clear();
				}
			}
		}

		// need to wait for all threads finish
		for(Thread trd : arr) {
			try {
				trd.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}


class Multiply1 implements Runnable{
	double[][] m1, m2, ret;
	int x, y, n;

	public Multiply1(double[][] m1, double[][] m2, double[][] ret, int x, int y) {
		this.m1 = m1;
		this.m2 = m2;
		this.ret = ret;
		this.x = x;
		this.y = y;
		n = m2.length;
	}

	@Override
	public void run() {
		ret[x][y] = 0;
		for(int k = 0; k < n ; k++) {
			ret[x][y] += (m1[x][k] * m2[k][y]);
		}
	}
}