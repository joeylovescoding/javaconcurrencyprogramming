import java.util.Random;
public class TestDriver {
	public static void main(String[] args) {
		int l = 1000;
		Random r = new Random();
		double[][] m1 = generateMatrix(l, l, r);
		double[][] m2 = generateMatrix(l, l, r);
		double[][] ret1 = new double[l][l];
		double[][] ret2 = new double[l][l];
		double[][] ret3 = new double[l][l];
		double[][] ret4 = new double[l][l];

		long start = 0;
		long end = 0;

		start = System.currentTimeMillis();
		SerialDriver s = new SerialDriver();
		s.multiply(m1, m2, ret1);
		end = System.currentTimeMillis();
		System.out.println("Serial: " + (end - start));

		start = System.currentTimeMillis();
		Driver1 d1 = new Driver1();
		d1.multiply(m1, m2, ret2);
		end = System.currentTimeMillis();
		System.out.println("each element: " + (end - start));

		start = System.currentTimeMillis();
		Driver2 d2 = new Driver2();
		d2.multiply(m1, m2, ret3);
		end = System.currentTimeMillis();
		System.out.println("each row: " + (end - start));

		start = System.currentTimeMillis();
		Driver3 d3 = new Driver3();
		d3.multiply(m1, m2, ret4);
		end = System.currentTimeMillis();
		System.out.println("based on # of processors: " + (end - start));
	}

	public static void printMatrix(double[][] m) {
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[0].length; j++) {
				System.out.printf("%.2f  ", m[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	public static double[][] generateMatrix(int l, int w, Random r){
		double[][] ret = new double[l][w];
		for(int i = 0; i < l; i++) {
			for(int j = 0; j < w; j++) {
				ret[i][j] = r.nextInt(100) + r.nextDouble();
			}
		}
		return ret;
	}
}

/*
	input of 100
	Serial: 11
	each element: 720
	each row: 21
	based on # of processors: 17

	input of 500
	Serial: 379
	each element: 15441
	each row: 341
	based on # of processors: 192


	input of 1000
	Serial: 4810
	each element: very long
	each row: 3731
	based on # of processors: 2473

	input of 2000
	Serial: 64125
	each element: very long
	each row: 36905
	based on # of processors: 26550
 */





