//this is the Serial version
public class SerialDriver implements Driverable{
	public void multiply(double[][] m1, double[][] m2, double[][] ret) {
		SerialMultiply r = new SerialMultiply(m1, m2, ret);
		r.multiply();
	}
}


class SerialMultiply{

	double[][] m1, m2, ret;
	int l, w, n;


	public SerialMultiply(double[][] m1, double[][] m2, double[][] ret) {
		this.m1 = m1;
		this.m2 = m2;
		this.ret = ret;
		l = m1.length;
		w = m2[0].length;
		n = m2.length;
	}

	// assume the matrix sizes are valid
	public void multiply() {
		for(int i = 0 ; i < l ; i++) {
			for(int j = 0; j < w; j++) {
				ret[i][j] = 0;
				for(int k = 0; k < n ; k++) {
					ret[i][j] += (m1[i][k] * m2[k][j]);
				}
			}
		}
	}
}