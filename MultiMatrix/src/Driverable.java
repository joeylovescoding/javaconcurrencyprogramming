public interface Driverable {
	void multiply(double[][] m1, double[][] m2, double[][] ret);
}
