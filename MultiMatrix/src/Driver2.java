import java.util.ArrayList;

//for each row create a Thread
public class Driver2 implements Driverable{
	@Override
	public void multiply(double[][] m1, double[][] m2, double[][] ret) {
		ArrayList<Thread> arr = new ArrayList<>();
		int l = m1.length;

		for(int i = 0; i < l; i++) {
			Thread t = new Thread(new Multiply2(m1, m2, ret, i));
			arr.add(t);
			t.start();

			// to avoid too many threads
			if(arr.size() == 10) {
				for(Thread trd : arr) {
					try {
						trd.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				arr.clear();
			}
		}

		// need to wait for all threads finish
		for(Thread trd : arr) {
			try {
				trd.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


class Multiply2 implements Runnable{
	double[][] m1, m2, ret;
	int x, y, n, w;

	public Multiply2(double[][] m1, double[][] m2, double[][] ret, int x) {
		this.m1 = m1;
		this.m2 = m2;
		this.ret = ret;
		n = m2.length;
		w = m2[0].length;
		this.x = x;
	}

	@Override
	public void run() {
		for(int j = 0; j < w; j++) {
			ret[x][j] = 0;
			for(int k = 0; k < n ; k++) {
				ret[x][j] += (m1[x][k] * m2[k][j]);
			}
		}
	}
}