package datautil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * Class that loads the examples of the Bank Marketing data set from a file
 * @author author
 * Copied from https://github.com/PacktPublishing/Mastering-Concurrency-Programming-with-Java-9-Second-Edition/blob/master/Chapter03/NearestNeighbors/src/com/javferna/packtpub/mastering/knn/loader/BankMarketingLoader.java
 * Given this class has nothing to do with concurrent programming
 * 
 */
public class DataLoader {

	/**
	 * Method that loads the examples of the Bank Marketing data set from a file
	 * @param dataPath Path to the file where the data items are stored
	 * @return List of BankMarketing examples
	 */
	public static List<Data> load (String dataPath) {
		Path file=Paths.get(dataPath);
		List<Data> dataSet=new ArrayList<>();
		try (InputStream in = Files.newInputStream(file);
				BufferedReader reader =
						new BufferedReader(new InputStreamReader(in))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String data[]=line.split(";");
				Data dataObject=new Data();
				dataObject.setData(data);
				dataSet.add(dataObject);
			}
		} catch (IOException x) {
			x.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataSet;
	}
}