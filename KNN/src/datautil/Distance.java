package datautil;

public class Distance implements Comparable<Distance>{
	
	// the original version in the book stores the index to retrieve the tag, 
	// which would use less memory, I just use tag here for convenience 
	public String tag;
	public double dis;

	public Distance(String tag, double dis) {
		this.tag = tag;
		this.dis = dis;
	}

	@Override
	public int compareTo(Distance o) {
		if(dis < o.dis)
			return -1;
		else if(dis > o.dis)
			return 1;
		else 
			return 0;
	}


}
