package datautil;
/**
 * The class to calculate the Euclidean Distance between two sample Data
 * 
 */


public class EucDisCalculator {
	public static double getDistance(Data d1, Data d2) {
		double ret = 0;
		double[] data1 = d1.getExample();
		double[] data2 = d2.getExample();

		for(int i = 0; i < data1.length; i++) {
			ret += Math.pow(data1[i]-data2[i], 2);
		}	
		return Math.sqrt(ret);
	}
}
