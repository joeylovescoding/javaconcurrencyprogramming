package classifier;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import datautil.Data;
import datautil.Distance;
import datautil.EucDisCalculator;

public class ConcurrentClassifierByProcessorNum extends Classifier{
	ThreadPoolExecutor executor;

	public ConcurrentClassifierByProcessorNum(Data[] training, int k) {
		super(training, k);
		int numOfThreads = java.lang.Runtime.getRuntime().availableProcessors();
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numOfThreads);
	}

	@Override
	public String classify(Data d) {
		int numOfThreads = java.lang.Runtime.getRuntime().availableProcessors();
		int step = training.length / numOfThreads;
		Distance[] ds = new Distance[training.length];
		CountDownLatch latch = new CountDownLatch(numOfThreads);

		int lo = 0;
		int hi = step;
		int i = 0;

		while(i < numOfThreads) {
			TaskByProcessorNum task = new TaskByProcessorNum(training, lo, hi, ds, d, latch);
			executor.execute(task);
			i++;
			lo = hi;
			hi += lo;
			if(i == numOfThreads - 1)
				hi = training.length;
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return getResult(ds);
	}

	public void destory() {
		executor.shutdown();
	}
}


class TaskByProcessorNum implements Runnable{
	Data[] training;
	int lo;
	int hi;
	Distance[] ret;
	Data d;
	CountDownLatch latch;

	public TaskByProcessorNum(Data[] training, int lo, int hi, Distance[] ret, Data d, CountDownLatch latch) {
		this.training = training;
		this.lo = lo;
		this.hi = hi;
		this.ret = ret;
		this.d = d;
		this.latch = latch;
	}

	@Override
	public void run() {
		for(int i = lo; i < hi; i++) {
			ret[i] = new Distance(training[i].getTag(), EucDisCalculator.getDistance(d, training[i]));
		}
		latch.countDown();
	}
}