package classifier;

import datautil.Data;
import datautil.Distance;
import datautil.EucDisCalculator;

public class SerialClassifier extends Classifier{

	public SerialClassifier(Data[] training, int k) {
		super(training, k);
	}


	public String classify(Data d) {
		Distance[] ds = new Distance[training.length];
		for(int i = 0 ; i < ds.length; i++) {
			ds[i] = new Distance(training[i].getTag(), 
					EucDisCalculator.getDistance(d, training[i]));
		}		

		return getResult(ds);
	}
}
