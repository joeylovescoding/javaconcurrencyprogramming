package classifier;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import datautil.Data;
import datautil.Distance;

public abstract class Classifier {
	Data[] training;
	int k;

	public Classifier(Data[] training, int k) {
		this.training = training;
		this.k = Math.min(k, training.length);
	}

	public abstract String classify(Data d);

	public String[] batchClassify(Data[] datas) {
		String[] ret = new String[datas.length];
		for(int i = 0; i < ret.length; i++) {
			ret[i] = classify(datas[i]);
		}
		return ret;
	}

	public String getResult(Distance[] ds) {
		// can also use Arrays.sort here, this is not the focus
		// there also might be better solution here, for example using heap
		// or first find Kth smallest and do a linear scan, but these solutions 
		// are beyond the scope of this project
		Arrays.parallelSort(ds);

		Map<String, Integer> map = new HashMap<>();
		Arrays.stream(ds)
		.limit(k)
		.forEach(k -> map.merge(k.tag, 1, (a, b)-> a+b));

		return Collections.max(map.entrySet(), Map.Entry.comparingByValue()).getKey();
	}

}
