package classifier;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import datautil.Data;
import datautil.Distance;
import datautil.EucDisCalculator;

public class ConcurrentClassifierByDataNum extends Classifier{
	ThreadPoolExecutor executor;

	public ConcurrentClassifierByDataNum(Data[] training, int k) {
		super(training, k);
		int numOfThreads = java.lang.Runtime.getRuntime().availableProcessors();
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numOfThreads);
	}

	public String classify(Data d) {
		Distance[] ds = new Distance[training.length];
		CountDownLatch latch = new CountDownLatch(ds.length);
		for(int i = 0 ; i < ds.length; i++) {
			TaskByDataNum task = new TaskByDataNum(training[i], i, ds, d, latch);
			executor.execute(task);
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return getResult(ds);
	}

	public void destory() {
		executor.shutdown();
	}
}

class TaskByDataNum implements Runnable{
	Data training;
	int index;
	Distance[] ret;
	Data d;
	CountDownLatch latch;

	public TaskByDataNum(Data training, int index, Distance[] ret, Data d, CountDownLatch latch) {
		this.training = training;
		this.index = index;
		this.ret = ret;
		this.d = d;
		this.latch = latch;
	}

	@Override
	public void run() {
		ret[index] = new Distance(training.getTag(), EucDisCalculator.getDistance(d, training));
		latch.countDown();
	}
}
