/**
 * Key points:
 * 	1. How to create a Executor: 
 * 		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numOfThreads);
 * 	2. How to execute new task:
 * 		executor.execute(task)
 *  3. How to use CountDownLatch:
 *  	latch.countDown(); latch.await();
 */

package test;

import java.util.List;

import classifier.ConcurrentClassifierByDataNum;
import classifier.ConcurrentClassifierByProcessorNum;
import classifier.SerialClassifier;
import datautil.Data;
import datautil.DataLoader;

public class TestClassfier {
	public static void main(String[] args) {
		List<Data> trainingList = DataLoader.load("data/bank.data.txt");
		List<Data> testingList = DataLoader.load("data/bank.test.txt");

		Data[] training = trainingList.stream().toArray(Data[]::new);
		Data[] testing = testingList.stream().toArray(Data[]::new);
		long start, end;

		start = System.currentTimeMillis();
		SerialClassifier s = new SerialClassifier(training, 10);
		String[] ret = s.batchClassify(testing);
		end = System.currentTimeMillis();
		System.out.println("SerialClassifier: " + (end - start));


		start = System.currentTimeMillis();
		ConcurrentClassifierByDataNum c1 = new ConcurrentClassifierByDataNum(training, 10);
		String[] ret1 = c1.batchClassify(testing);
		c1.destory();
		end = System.currentTimeMillis();
		System.out.println("ConcurrentClassifierByDataNum: " + (end - start));

		start = System.currentTimeMillis();
		ConcurrentClassifierByProcessorNum c2 = new ConcurrentClassifierByProcessorNum(training, 10);
		String[] ret2 = c2.batchClassify(testing);
		c2.destory();
		end = System.currentTimeMillis();
		System.out.println("ConcurrentClassifierByProcessorNum: " + (end - start));
	}

}

/*
SerialClassifier: 39450
ConcurrentClassifierByDataNum: 57872  //seems overhead too high for creating task
ConcurrentClassifierByProcessorNum: 31355
*/
