import java.io.File;

public class SerialFileSearch {
	public String search(File f, String target) {
		if(f == null)
			return null;

		File[] files = f.listFiles();
		if(files == null)
			return null;

		for(File file : files) {
			if(file.isDirectory()) {
				String t = search(file, target);
				if(t != null)
					return t;
			}
			else {
				if(file.getName().equalsIgnoreCase(target)) {
					return file.getAbsolutePath();
				}
			}
		}
		return null;
	}
}
