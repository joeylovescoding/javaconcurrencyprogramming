import java.io.File;
import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ConcurrentSearch implements Runnable{
	String target;
	ConcurrentLinkedQueue<File> queue;
	Result ret;
	int count = 0;

	public ConcurrentSearch(String target, ConcurrentLinkedQueue<File> queue, Result ret) {
		this.target = target;
		this.queue = queue;
		this.ret = ret;
	}

	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()) {
			if(count == 5) {
				return;
			}
				

			if(queue.size() == 0) {
				// this is to avoid a thread get terminated too quickly and 
				// then it ends up to be a serial one
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				count++;
				continue;
			}

			File file = queue.poll();
			if(file == null)
				continue;

			File[] files = file.listFiles();
			if(files == null || files.length == 0)
				continue;

			for(File f : files) {
				if(f.isDirectory()) {
					queue.offer(f);
				}
				else {
					if(f.getName().equalsIgnoreCase(target)) {
						ret.setRet(f.getAbsolutePath());
						return;
					}
				}
			}
		}
	}



}
class ConcureentSearchDriver{
	String search(File f, String target) {
		if(f.isFile()) {
			return f.getName().equalsIgnoreCase(target) ? f.getAbsolutePath() : null;
		}

		ConcurrentLinkedQueue<File> queue = new ConcurrentLinkedQueue<>();
		int num = java.lang.Runtime.getRuntime().availableProcessors();
		queue.offer(f);
		Result result = new Result();
		ArrayList<Thread> arr= new ArrayList<Thread>();
		for(int i = 0; i < num; i++) {
			Thread thread = new Thread(new ConcurrentSearch(target, queue, result));
			arr.add(thread);
			thread.start();
		}
		
		boolean bFound = false;
		while(!bFound) {
			int terminated = 0;
			for(int i = 0 ; i < num; i++) {
				Thread thread = arr.get(i);
				if(thread.getState() == State.TERMINATED) {
					terminated += 1;
					if(result.ret != null) {
						bFound = true;
						break;
					}
				}
			}
			
			if(terminated == num) {
				break;
			}
		}
		
		// if the result is found, need to interrupt other threads
		if(bFound) {
			for(int i = 0 ; i < num; i++) {
				arr.get(i).interrupt();
			}
			
			for(Thread thread : arr) {
				try {
					thread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		// else if all the Threads are TERMINATED, do nothing

		return result.ret;
	}
}


class Result{
	String ret = null;
	synchronized void setRet(String ret) {
		if(this.ret == null) {
			this.ret = ret;
		}
	}
}