import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.AssumptionViolatedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Stopwatch;
import org.junit.rules.Timeout;
import org.junit.runner.Description;

public class UnitTestSearch {

	@Rule
	public Timeout globalTimeout = Timeout.seconds(100);

	@Rule
	public final Stopwatch stopwatch = new Stopwatch() {

		void printInfo(long nanos, Description description, String status) {
			String testName = description.getMethodName();
			System.out.println(String.format("Test %s %s, spent %d microseconds",
					testName, status, TimeUnit.NANOSECONDS.toMicros(nanos)));
		}

		protected void succeeded(long nanos, Description description) {
			printInfo(nanos, description, "succeeded");
		}

		protected void failed(long nanos, Throwable e, Description description) {
			printInfo(nanos, description, "failed");
		}

		protected void skipped(long nanos, AssumptionViolatedException e,
				Description description) {
			printInfo(nanos, description, "skipped");
		}

		protected void finished(long nanos, Description description) {
			printInfo(nanos, description, "finished");
			System.out.println();
		}

	};

	static File f = new File("//./Applications");
	static SerialFileSearch sf = new SerialFileSearch();
	static ConcureentSearchDriver cf = new ConcureentSearchDriver();
	static String target = "LIESMICH.rt";

	@Test
	public void testSerial() {
		String retString = sf.search(f, target);
		System.out.println(retString);
	}

	@Test
	public void testConcurrent() {
		String retString = cf.search(f, target);
		System.out.println(retString);
	}
}

/*
/./Applications/MAMP/LIESMICH.rtf
Test testSerial succeeded, spent 876076 microseconds
Test testSerial finished, spent 876076 microseconds

/./Applications/MAMP/LIESMICH.rtf
Test testConcurrent succeeded, spent 5160 microseconds
Test testConcurrent finished, spent 5160 microseconds



null
Test testSerial succeeded, spent 8123161 microseconds
Test testSerial finished, spent 8123161 microseconds

null
Test testConcurrent succeeded, spent 6198656 microseconds
Test testConcurrent finished, spent 6198656 microseconds
 */

