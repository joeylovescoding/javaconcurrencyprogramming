import java.io.File;

public class TestSearch {

	public static void main(String[] args) {
		File f = new File("//./Applications");

		long start = 0;
		long end = 0;


		SerialFileSearch sf = new SerialFileSearch();

		start = System.currentTimeMillis();
		String ret1 = sf.search(f, "LIESMICH.rtf");
		System.out.println(ret1);
		end = System.currentTimeMillis();
		System.out.println("Serial: " + (end - start));

		ConcureentSearchDriver cf = new ConcureentSearchDriver();
		start = System.currentTimeMillis();
		String ret2 = cf.search(f, "LIESMICH.rtf");
		System.out.println(ret2);
		end = System.currentTimeMillis();
		System.out.println("Concurrent: " + (end - start));
	}

}

/*

We can find concurrent one much quicker in this case

/./Applications/MAMP/LIESMICH.rtf
Serial: 889
/./Applications/MAMP/LIESMICH.rtf
Concurrent: 4

 */
